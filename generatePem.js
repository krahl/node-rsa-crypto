const { generateKeyPairSync } = require('crypto');
const { publicKey, privateKey } = generateKeyPairSync('rsa', {
  modulusLength: 4096,
  publicKeyEncoding: {
    type: 'spki',
    format: 'pem'
  },
  privateKeyEncoding: {
    type: 'pkcs8',
    format: 'pem',
    cipher: 'aes-256-cbc',
    passphrase: 'krahl'
  }
});

console.log("Public --------------------------------")
console.log(publicKey);
console.log("Public --------------------------------")

console.log("Private --------------------------------")
console.log(privateKey);
console.log("Private --------------------------------")